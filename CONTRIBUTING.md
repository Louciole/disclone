# 🎉 Contributing to Disclone

Thank you for considering contributing to Disclone! Your help is invaluable in making this project a success.

- GitLab: [GitLab Link](https://gitlab.com/Louciole/disclone/)
- GitHub [Mirror]: [GitHub Link](https://github.com/Louciole/disclone)
- Issues: [GitLab Issues](https://gitlab.com/Louciole/disclone/-/issues)
- Documentation: [Sakura Docs](https://gitlab.com/Louciole/sakura-docs)
- Documentation Repo: [GitLab](https://gitlab.com/Louciole/sakura-docs)

---

## 📋 Table of Contents

1. [Task List](#task-list)
2. [Process](#process)
3. [Style](#style)
4. [Issues and Feedback](#issues-and-feedback)

---

## ✅ Task List

Check out our to-do list on Smooth Projects: [Smooth Projects Link](https://smoop.carbonlab.dev/project?uid=6&&access=read-only)

---

## 🚀 Process

1. **Choose something to work on**: Whether it's a bug fix or a new feature, you can make an impact. Need help? Contact [lou@carbonlab.dev](mailto:lou@carbonlab.dev).
2. **Fork the repository**: Create your own copy of the project.
3. **Open a Merge Request**: Submit your changes on gitlab for review.

---

## 🎨 Style

- **Commit messages** should be funny and contain at least one emoji. 😄
- **Code** should be as simple and minimalistic as possible.

---

## 🐛 Issues and Feedback

Found a bug? Have an amazing feature idea? Share your thoughts by [opening an issue on GitLab](https://gitlab.com/Louciole/disclone/-/issues).